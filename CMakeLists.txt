list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
project(rpi-daq-analyzerr)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall")

find_package(Boost COMPONENTS thread system REQUIRED)
find_package(ROOT REQUIRED)
FOREACH( pkg ROOT)
    IF( ${pkg}_FOUND )
        INCLUDE_DIRECTORIES( ${${pkg}_INCLUDE_DIRS} )
        LINK_LIBRARIES( ${${pkg}_LIBRARIES} )
        ADD_DEFINITIONS ( ${${pkg}_DEFINITIONS} )
    ENDIF()
ENDFOREACH()

include_directories(SYSTEM ${CMAKE_CURRENT_SOURCE_DIR}/include ${Boost_INCLUDE_DIRS} )
link_directories( ${link_directories} ${EXTERN_BOOST_LIB_PREFIX}  )

file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cc)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.h)

add_executable(hexaboardAnalyzer src/hexaboardAnalyzer.cxx ${sources} ${headers})

target_link_libraries(hexaboardAnalyzer ${Boost_LIBRARIES} boost_program_options boost_serialization)

install(TARGETS hexaboardAnalyzer DESTINATION ${PROJECT_SOURCE_DIR}/bin
	ARCHIVE DESTINATION ${PROJECT_SOURCE_DIR}/lib
	LIBRARY DESTINATION ${PROJECT_SOURCE_DIR}/lib)
