#include <pedestalAnalyzer.h>
#include <pedestalData.h>

#include <sstream>
#include <algorithm>

#define IQR_TO_SIGMA 0.6745

pedestalAnalyzer::pedestalAnalyzer(std::string outputDir, std::string outputname, std::string mapfile, int maxTS)
{
  std::ostringstream os( std::ostringstream::ate );

  os.str("");
  os << outputDir << "/pedestal_" << outputname << ".ar";
  m_pedestalFile = os.str();

  os.str("");
  os << outputDir << "/pedestal_" << outputname << ".txt";
  m_pedestalFileForHexPlot = os.str();

  m_maxTS=maxTS;

  FILE* f = fopen(mapfile.c_str(), "r");
  if (f == 0) {
    fprintf(stderr, "Unable to open '%s'\n", mapfile.c_str() );
    exit(1);
  }
  char buffer[100];
  while (!feof(f)) {
    buffer[0] = 0;
    fgets(buffer, 100, f);
    if( std::string(buffer).find('#') != std::string::npos )
      continue;
    printf(buffer);
    // for( int i=0; i<0; i++)
    //   std::cout << std::string(1,buffer[i]) << " ";
    // std::cout << std::endl;
    // char* p_comment = index(buffer, '#');
    // if (p_comment != 0) *p_comment = 0;
    int ptr;
    int padID, chipID, chanID;
    const char* process = buffer;
    int found = sscanf(buffer, "%d %d %d %n ", &padID, &chipID, &chanID, &ptr);
    if (found == 3) {
      process += ptr;
    } else continue;
    chanID_to_padID ctp;
    ctp.chanID=chanID;
    ctp.chipID=chipID;
    ctp.padID=padID;
    m_padMap.insert( std::pair<int,chanID_to_padID>(padID,ctp) );
  }
}

void pedestalAnalyzer::fillData( const std::vector<skiroc2cms>& skirocs )
{
  int chip=0;
  for( auto skiroc : skirocs ){
    //std::cout << skiroc << std::endl;
    std::vector<int> timesamples=skiroc.rollPositions();
    for( unsigned int ichan=0; ichan<N_CHANNELS_PER_SKIROC; ichan++){
      for( unsigned int isca=0; isca<NUMBER_OF_SCA; isca++ ){
	if( timesamples[isca]>m_maxTS )
	  continue;
	int key=100*100*chip+100*ichan+isca;
	if( sca_data_map.find(key)==sca_data_map.end() ){
	  sca_channel_data scd(key);
	  sca_data_map.insert( std::pair<int,sca_channel_data>(key,scd) );
	}
	sca_data_map[key].highGain.push_back( skiroc.ADCHigh(ichan,isca) );
	sca_data_map[key].lowGain.push_back(  skiroc.ADCLow(ichan,isca)  );
      }
    }
    chip++;
  }
  //getchar();
}

void pedestalAnalyzer::analyze()
{
  pedestalData _pedData;
  for( auto scd : sca_data_map ){
    sca_channel_data sca_channel=scd.second;
    int chip = sca_channel.key/100/100;
    int chan = (sca_channel.key/100)%100;
    
    int padID=-1;
    for(auto pad:m_padMap)
      if( pad.second.chipID==chip && pad.second.chanID==chan ){
    	padID=pad.second.padID;
    	break;
      }

    if( _pedData.contain(chip,chan)==false )
      _pedData.fillData(chip,chan,padID);

    std::vector<uint16_t> hg=sca_channel.highGain;
    std::vector<uint16_t> lg=sca_channel.lowGain;
    std::sort( hg.begin(), hg.end() );
    std::sort( lg.begin(), lg.end() );

    unsigned int size = hg.size();
    int medianIndex = int( 0.5*(size-1) );
    int sigma_1_Index = int( 0.25*(size-1) );
    int sigma_3_Index = int( 0.75*(size-1) );
    sca_channel.medianHG = hg[ medianIndex ];
    sca_channel.medianLG = lg[ medianIndex ];
    
    sca_channel.iqrHG=0.5*( hg[ sigma_3_Index ]-hg[ sigma_1_Index ])/IQR_TO_SIGMA;
    sca_channel.iqrLG=0.5*( lg[ sigma_3_Index ]-lg[ sigma_1_Index ])/IQR_TO_SIGMA;
    
    int sca  = sca_channel.key%100;
    auto p = _pedData.get(chip,chan);
    p.setPedestal(1,sca,sca_channel.medianHG);
    p.setPedestal(0,sca,sca_channel.medianLG);
    p.setNoise   (1,sca,sca_channel.iqrHG);
    p.setNoise   (0,sca,sca_channel.iqrLG);
    _pedData.update(p);
 
  }

  pedestalArchiver ar;
  ar.savePedestalData( _pedData,m_pedestalFile );

  std::ofstream out;
  out.open( m_pedestalFileForHexPlot.c_str(), std::ios::out );
  out << "## selector (0-12 pedestal LG, 100-112 pedestalHG, 200-212 noise LG, 300-312 noise HG ) pad value" << std::endl;
  // PEDESTAL LG
  for( unsigned int isca=0; isca<NUMBER_OF_SCA; isca++ ){
    for( auto pcd : _pedData.get() )
      if( pcd.sensorpad()!=-1 )
	out << isca << "\t" << pcd.sensorpad() << "\t" << pcd.getPedestal(0,isca) << std::endl;
  }
  // PEDESTAL HG
  for( unsigned int isca=0; isca<NUMBER_OF_SCA; isca++ ){
    for( auto pcd : _pedData.get() )
      if( pcd.sensorpad()!=-1 )
	out << isca+100 << "\t" << pcd.sensorpad() << "\t" << pcd.getPedestal(1,isca) << std::endl;
  }
  // NOISE LG
  for( unsigned int isca=0; isca<NUMBER_OF_SCA; isca++ ){
    for( auto pcd : _pedData.get() )
      if( pcd.sensorpad()!=-1 )
	out << isca+200 << "\t" << pcd.sensorpad() << "\t" << pcd.getNoise(0,isca) << std::endl;
  }
  // NOISE HG
  for( unsigned int isca=0; isca<NUMBER_OF_SCA; isca++ ){
    for( auto pcd : _pedData.get() )
      if( pcd.sensorpad()!=-1 )
	out << isca+300 << "\t" << pcd.sensorpad() << "\t" << pcd.getNoise(1,isca) << std::endl;
  }
  out.close();
}
