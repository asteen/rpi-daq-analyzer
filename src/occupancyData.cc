#include <algorithm>

#include "occupancyData.h"

occupancyChanData::occupancyChanData(int chip,int channel)
{
  m_chip=chip;
  m_channel=channel;
  m_nhits=0;
}

occupancyChanData::occupancyChanData(int chip,int channel, int sensorpad)
{
  m_chip=chip;
  m_channel=channel;
  m_sensorpad=sensorpad;
  m_nhits=0;
}


occupancyChanData& occupancyData::get(int chip,int channel)
{
  occupancyChanData ocd(chip,channel);
  return (*std::find( m_datas.begin(), m_datas.end(), ocd ));
}

bool occupancyData::contain(int chip,int channel)
{
  occupancyChanData ocd(chip,channel);
  return std::find( m_datas.begin(), m_datas.end(), ocd )!=m_datas.end() ;
}

void occupancyData::fillData(int chip, int channel,int sensorpad)
{
  occupancyChanData ocd(chip,channel,sensorpad);
  m_datas.push_back( ocd );
}

void occupancyData::update(occupancyChanData ocd)
{
  auto it = std::find( m_datas.begin(), m_datas.end(), ocd );
  if( it==m_datas.end() )
    std::cout << "occupancy channel " << ocd << "was not found in occupancyData container" << std::endl;
  else 
    (*it)=ocd;
}
