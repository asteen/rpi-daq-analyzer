#include <algorithm>

#include "pedestalData.h"

pedestalChanData::pedestalChanData(int chip,int channel)
{
  m_chip=chip;
  m_channel=channel;
  m_pedestalHG.reserve(NUMBER_OF_SCA);
  m_pedestalLG.reserve(NUMBER_OF_SCA);
  m_noiseHG.reserve(NUMBER_OF_SCA);
  m_noiseLG.reserve(NUMBER_OF_SCA);
  m_pedestalHG = std::vector<float>(NUMBER_OF_SCA,0);
  m_pedestalLG = std::vector<float>(NUMBER_OF_SCA,0);
  m_noiseHG    = std::vector<float>(NUMBER_OF_SCA,0);
  m_noiseLG    = std::vector<float>(NUMBER_OF_SCA,0);
}

pedestalChanData::pedestalChanData(int chip,int channel, int sensorpad)
{
  m_chip=chip;
  m_channel=channel;
  m_sensorpad=sensorpad;
  m_pedestalHG.reserve(NUMBER_OF_SCA);
  m_pedestalLG.reserve(NUMBER_OF_SCA);
  m_noiseHG.reserve(NUMBER_OF_SCA);
  m_noiseLG.reserve(NUMBER_OF_SCA);
  m_pedestalHG = std::vector<float>(NUMBER_OF_SCA,0);
  m_pedestalLG = std::vector<float>(NUMBER_OF_SCA,0);
  m_noiseHG    = std::vector<float>(NUMBER_OF_SCA,0);
  m_noiseLG    = std::vector<float>(NUMBER_OF_SCA,0);
}


pedestalChanData& pedestalData::get(int chip,int channel)
{
  pedestalChanData pcd(chip,channel);
  return (*std::find( m_datas.begin(), m_datas.end(), pcd ));
}

bool pedestalData::contain(int chip,int channel)
{
  pedestalChanData pcd(chip,channel);
  return std::find( m_datas.begin(), m_datas.end(), pcd )!=m_datas.end() ;
}

void pedestalData::fillData(int chip, int channel,int sensorpad)
{
  pedestalChanData pcd(chip,channel,sensorpad);
  m_datas.push_back( pcd );
}

void pedestalData::update(pedestalChanData pcd)
{
  auto it = std::find( m_datas.begin(), m_datas.end(), pcd );
  if( it==m_datas.end() )
    std::cout << "pedestal channel " << pcd << "was not found in pedestalData container" << std::endl;
  else 
    (*it)=pcd;
}
