#include "skiroc2cms.h"

skiroc2cms::skiroc2cms( const uint16_t* data )
{
  m_data = (uint16_t*)malloc(SKIROC_DATA_SIZE*sizeof(*m_data));
  memcpy(m_data,data,SKIROC_DATA_SIZE*sizeof(*m_data));
}

uint16_t skiroc2cms::gray_to_binary(const uint16_t gray) const
{
  uint16_t result = gray & (1 << 11);
  result |= (gray ^ (result >> 1)) & (1 << 10);
  result |= (gray ^ (result >> 1)) & (1 << 9);
  result |= (gray ^ (result >> 1)) & (1 << 8);
  result |= (gray ^ (result >> 1)) & (1 << 7);
  result |= (gray ^ (result >> 1)) & (1 << 6);
  result |= (gray ^ (result >> 1)) & (1 << 5);
  result |= (gray ^ (result >> 1)) & (1 << 4);
  result |= (gray ^ (result >> 1)) & (1 << 3);
  result |= (gray ^ (result >> 1)) & (1 << 2);
  result |= (gray ^ (result >> 1)) & (1 << 1);
  result |= (gray ^ (result >> 1)) & (1 << 0);
  return result;
}

std::vector<int> skiroc2cms::rollPositions() const
{
  std::bitset<NUMBER_OF_SCA> bitstmp=rollMask();
  std::bitset<NUMBER_OF_SCA> bits;
  for( size_t i=0; i<NUMBER_OF_SCA; i++ )
    bits[i]=bitstmp[12-i];
  std::vector<int> rollpositions(NUMBER_OF_SCA);
  if(bits.test(0)&&bits.test(12)){
    rollpositions[0]=12;
    for(size_t i=1; i<NUMBER_OF_SCA; i++)
      rollpositions[i]=(i-1);
  }
  else{
    int pos_trk1 = -1;
    for(size_t i=0; i<NUMBER_OF_SCA; i++)
      if(bits.test(i)){
	pos_trk1 = i;
	break;
      }
    for(size_t i=0; i<NUMBER_OF_SCA; i++)
      if( (int)i <= pos_trk1 + 1 )
	rollpositions[i]=i + 12 - (pos_trk1 + 1);
      else
	rollpositions[i]=i - 1 - (pos_trk1 + 1);
  }
  return rollpositions;
}

bool skiroc2cms::check(bool printErrors)
{
  for( size_t j=0; j<N_CHANNELS_PER_SKIROC; j++ ){
    uint16_t head=(m_data[j]&MASK_HEAD)>>4*3;
    if(head!=8&&head!=9){
      if( printErrors ) std::cout << "ISSUE : we expected 8(1000) or 9(1001) for the adc header and I find " << head << std::endl;
      return false;
    }
    for( size_t k=0; k<NUMBER_OF_SCA+1; k++){
      if( ((m_data[j+SCA_SHIFT*k]&MASK_HEAD)>>4*3)!=head ){
	if( printErrors ) std::cout << "\n We have a major issue (LG)-> " << head << " should be the same as " << ((m_data[j+SCA_SHIFT*k]&MASK_HEAD)>>4*3) << std::endl;
	return false;
      }
    }
    head=(m_data[j+N_CHANNELS_PER_SKIROC]&MASK_HEAD)>>4*3;
    if(head!=8&&head!=9){
      if( printErrors ) std::cout << "ISSUE : we expected 8(1000) or 9(1001) for the adc header and I find " << head << std::endl;
      return false;
    }
    for( size_t k=0; k<NUMBER_OF_SCA+1; k++){
      if( ((m_data[j+SCA_SHIFT*k+N_CHANNELS_PER_SKIROC]&MASK_HEAD)>>4*3)!=head ){
	if( printErrors ) std::cout << "\n We have a major issue (HG)-> " << head << " should be the same as " << ((m_data[j+SCA_SHIFT*k+N_CHANNELS_PER_SKIROC]&MASK_HEAD)>>4*3) << std::endl;
	return false;
      }
    }
  }
  return true;
}
