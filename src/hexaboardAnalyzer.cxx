#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <stdio.h>
#include <limits>
#include <boost/program_options.hpp>

#include "unpacker.h"
#include "ntupler.h"
#include "pedestalAnalyzer.h"
#include "triggerHitProducer.h"
#include "commonmode.h"
#include "occupancyAnalyzer.h"
#include "noiseAnalyzer.h"
#include "correlationAnalyzer.h"

int main(int argc,char**argv)
{
  uint32_t m_headerSize;
  uint32_t m_eventTrailerSize;
  uint32_t m_nSkipEvents;
  uint32_t m_maxEvents;
  bool m_compressedData,m_saveNtupleOutput;
  int m_maxTS,m_expectedMaxTS,m_threshold,m_maxNHits ;
  std::string m_fileName,m_outputName,m_mapName,m_outputDir,m_analysis,m_hexaboardType,m_pedestalFileName;
  try { 
    /** Define and parse the program options 
     */ 
    namespace po = boost::program_options; 
    po::options_description generic_options("Generic options"); 
    generic_options.add_options()
      ("help,h", "Print help messages")
      ("fileName,f", po::value<std::string>(&m_fileName)->default_value("moduletest.raw"), "name of input file")
      ("analysis,a", po::value<std::string>(&m_analysis)->default_value("pedestal"), "analysis procedure to run, available options : pedestal, occupancy, correlation")
      ("saveNtupleOutput,s", po::value<bool>(&m_saveNtupleOutput)->default_value(false), "flag to save the raw data root ntuple")
      ("outputDir,O", po::value<std::string>(&m_outputDir)->default_value("results"), "name of output directory where output files will be stored")
      ("outputName,o", po::value<std::string>(&m_outputName)->default_value("moduletest"), "prefix name to build output files");

    po::options_description daq_options("DAQ options"); 
    daq_options.add_options()
      ("headerSize",po::value<uint32_t>(&m_headerSize)->default_value(192),"size (bytes) of the header")
      ("eventTrailerSize",po::value<uint32_t>(&m_eventTrailerSize)->default_value(2),"size (bytes) of the event trailer")
      ("compressedData",po::value<bool>(&m_compressedData)->default_value(true),"option to set if the data have been compressed");

    po::options_description analysis_options("Global analysis options"); 
    analysis_options.add_options()
      ("maxEvents,N",po::value<uint32_t>(&m_maxEvents)->default_value(std::numeric_limits<uint32_t>::max()),"maximum number of event to process")
      ("nSkipEvents,n",po::value<uint32_t>(&m_nSkipEvents)->default_value(1),"number of event to skip (better to skip 1st event)")
      ("hexaboardType",po::value<std::string>(&m_hexaboardType)->default_value("8inch"),"flag to know if the data comes from 6 or 8 inch sk2cms module (important for the mapping)")
      ("padMapFileName,m", po::value<std::string>(&m_mapName)->default_value("map.csv"), "pad to channel mapping file name");

    po::options_description pedestal_options("Pedestal analysis options"); 
    pedestal_options.add_options()
      ("maxTS", po::value<int>(&m_maxTS)->default_value(0), "maximum value of time sample to include in the pedestal estimations");

    po::options_description occupancy_options("Occupancy/correlation analysis options"); 
    occupancy_options.add_options()
      ("pedestalFileName,p", po::value<std::string>(&m_pedestalFileName)->default_value("pedestal.ar"), "boost archive file name containing pedestal data")
      ("threshold", po::value<int>(&m_threshold)->default_value(20), "minimum value in ADC counts to consider the reconstructed signal as a hit")
      ("expectedMaxTS", po::value<int>(&m_expectedMaxTS)->default_value(3), "expectation of the position of the maximum of the pulse shapes (in time sample unit: 25 ns bin)")
      ("maxNHits", po::value<int>(&m_maxNHits)->default_value(10), "maximum number of hits per event in order to consider the event valid");

    po::options_description cmdline_options;
    cmdline_options.add(generic_options).add(daq_options).add(analysis_options).add(pedestal_options).add(occupancy_options);

    po::variables_map vm; 
    try { 
      po::store(po::parse_command_line(argc, argv, cmdline_options),  vm); 
      if ( vm.count("help")  ) { 
        std::cout << generic_options   << std::endl; 
        std::cout << daq_options       << std::endl; 
	std::cout << analysis_options  << std::endl;
	std::cout << pedestal_options  << std::endl;
	std::cout << occupancy_options << std::endl;
        return 0; 
      } 
      po::notify(vm);
    }
    catch(po::error& e) { 
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
      std::cerr << generic_options << std::endl; 
      return 1; 
    }
    
    std::cout << std::endl;
    if( vm.count("fileName") )          std::cout << "fileName = "           << m_fileName << std::endl;
    if( vm.count("analysis") && (!(m_analysis == "pedestal" || m_analysis == "occupancy" || m_analysis == "correlation")) ){
      std::cout << "Invalid choice for option 'analysis'. Allowed choices : 'pedestal', 'occupancy', 'correlation'" << std::endl;
      throw po::validation_error(po::validation_error::invalid_option_value, "analysis");
    }
    if( vm.count("analysis")  )         std::cout << "m_analysis = "         << m_analysis << std::endl;

    if( vm.count("saveNtupleOutput")  ) std::cout << "m_saveNtupleOutput = " << m_saveNtupleOutput << std::endl;
    if( vm.count("outputDir") )         std::cout << "outputDir = "          << m_outputDir << std::endl;
    if( vm.count("outputName") )        std::cout << "outputName = "         << m_outputName << std::endl;
    std::cout << std::endl;

    if( vm.count("headerSize") )        std::cout << "m_headerSize = "       << m_headerSize << std::endl;
    if( vm.count("eventTrailerSize") )  std::cout << "eventTrailerSize = "   << m_eventTrailerSize << std::endl;
    if( vm.count("compressedData") )    std::cout << "compressedData= "      << m_compressedData << std::endl;
    std::cout << std::endl;
    
    if( vm.count("maxEvents") )         std::cout << "maxEvents= "           << m_maxEvents << std::endl;
    if( vm.count("nSkipEvents") )       std::cout << "nSkipEvents = "        << m_nSkipEvents << std::endl;
    if( vm.count("hexaboardType") && (!(m_hexaboardType == "8inch" || m_hexaboardType == "6inch")) ){
      std::cout << "Invalid choice for option 'hexaboardType'. Allowed choices : '6inch', '8inch'" << std::endl;
      throw po::validation_error(po::validation_error::invalid_option_value, "hexaboardType");
    }
    if( vm.count("hexaboardType") )     std::cout << "hexaboardType = "      << m_hexaboardType << std::endl;
    if( vm.count("padMapFileName") )    std::cout << "padMapFileName = "     << m_mapName << std::endl;
    std::cout << std::endl;

    if( vm.count("maxTS") )             std::cout << "maxTS = "              << m_maxTS << std::endl;
    if( vm.count("pedestalFileName") )  std::cout << "pedestalFileName = "   << m_pedestalFileName << std::endl;
    if( vm.count("threshold") )         std::cout << "threshold = "          << m_threshold << std::endl;
    if( vm.count("expectedMaxTS") )     std::cout << "expectedMaxTS = "      << m_expectedMaxTS << std::endl;
    std::cout << std::endl;
  }
  catch(std::exception& e) { 
    std::cerr << "Unhandled Exception reached the top of main: " 
              << e.what() << ", application will now exit" << std::endl; 
    return 2; 
  } 

  unpacker _unpacker(m_fileName,
  		     m_headerSize,
  		     m_eventTrailerSize,
  		     m_nSkipEvents, 
  		     m_maxEvents, 
  		     m_compressedData,
		     m_hexaboardType);
  
  unsigned int m_event=0;
  if( m_analysis=="pedestal" ){
    pedestalAnalyzer _pedestalAnalyzer(m_outputDir,m_outputName,m_mapName,m_maxTS);
    auto _ntupler = std::make_shared<rawntupler>();
    if( m_saveNtupleOutput ){  
      _ntupler = std::make_shared<rawntupler>(m_outputDir,m_outputName,std::string("sk2cms"), std::string("sk2cms hexaboard raw data tree"), m_mapName);
    }
    while(1){
      std::vector<skiroc2cms> skirocs;
      if( !_unpacker.unpack(skirocs) || m_event>=m_maxEvents ) 
	break;
      else{
	bool checker=true;
	for( auto skiroc : skirocs ){
	  checker = skiroc.check(0);
	  if (checker==false){
	    std::cout << "Event " << std::dec << m_event << " has data corruption -> it will be skipped " << std::endl;
	    break;
	  }
	}
	if (checker==false) {	
	  m_event++;
	  continue; //we skip the event if a data corruption is found
	}
	if( m_event%500==0 ) std::cout << "Processed " << std::dec << m_event << " events\t" << skirocs.size() << std::endl;
	if( m_saveNtupleOutput )  
	  _ntupler->fill(skirocs);
      	_pedestalAnalyzer.fillData(skirocs);
	m_event++;
      }
    }
    _pedestalAnalyzer.analyze();
  }
  else if( m_analysis=="correlation" ){
    triggerHitProducer prod(m_pedestalFileName);
    correlationAnalyzer _correlationAnalyzer(m_outputDir,m_outputName,m_mapName);;
    while(1){
      std::vector<skiroc2cms> skirocs;
      if( !_unpacker.unpack(skirocs) || m_event>=m_maxEvents ) 
	break;
      else{
	bool checker=true;
	for( auto skiroc : skirocs ){
	  checker = skiroc.check(0);
	  if (checker==false){
	    std::cout << "Event " << std::dec << m_event << " has data corruption -> it will be skipped " << std::endl;
	    break;
	  }
	}
	if (checker==false) {	
	  m_event++;
	  continue; //we skip the event if a data corruption is found
	}
	std::vector<triggerHit> hits;
      	prod.produceTriggerHits(skirocs,hits);
      	_correlationAnalyzer.fillData(hits);
	if( m_event%500==0 ) std::cout << "Processed " << std::dec << m_event << " events\t" << skirocs.size() << "\t" << hits.size() << std::endl;
	m_event++;
      }
    }
    _correlationAnalyzer.analyze();    
  }
  else if( m_analysis=="occupancy" ){
    triggerHitProducer prod(m_pedestalFileName);
    commonmode cm(m_mapName,m_expectedMaxTS,m_threshold);
    occupancyAnalyzer _occupancyAnalyzer(m_outputDir,m_outputName,m_mapName,m_threshold,m_expectedMaxTS,m_maxNHits);
    //noiseAnalyzer _noiseAnalyzer(m_outputDir,m_outputName,m_mapName);
    auto _ntupler = std::make_shared<triggerhitntupler>();
    if( m_saveNtupleOutput ){  
      _ntupler = std::make_shared<triggerhitntupler>(m_outputDir,m_outputName,std::string("triggerhits"), std::string("skiroc2cms hexaboard trigger hit tree"), m_mapName);
    }
    while(1){
      std::vector<skiroc2cms> skirocs;
      if( !_unpacker.unpack(skirocs) || m_event>=m_maxEvents ) 
	break;
      else{
	bool checker=true;
	for( auto skiroc : skirocs ){
	  checker = skiroc.check(0);
	  if (checker==false){
	    std::cout << "Event " << std::dec << m_event << " has data corruption -> it will be skipped " << std::endl;
	    break;
	  }
	}
	if (checker==false) {	
	  m_event++;
	  continue; //we skip the event if a data corruption is found
	}
	std::vector<triggerHit> hits;
      	prod.produceTriggerHits(skirocs,hits);
	cm.evalAndSubtractCommonMode(hits);
        _occupancyAnalyzer.fillData(hits);
	if( m_saveNtupleOutput )  
	  _ntupler->fill(hits);	
      	//_noiseAnalyzer.fillData(hits);
	if( m_event%500==0 ) std::cout << "Processed " << std::dec << m_event << " events\t" << skirocs.size() << "\t" << hits.size() << std::endl;
	m_event++;
      }
    }
    _occupancyAnalyzer.analyze();
    //_noiseAnalyzer.analyze();
  }
  else return 0;
  
}
