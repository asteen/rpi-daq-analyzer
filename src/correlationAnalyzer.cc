#include <correlationAnalyzer.h>

#include <sstream>
#include <algorithm>
#include <boost/range/adaptors.hpp>

channel_data::channel_data()
{
  hgmean=0.;
  hgrms=0.;
  lgmean=0.;
  lgrms=0.;
}

channel_data::channel_data(int _chip,int _chan, int _pad) : 
  chip(_chip),
  chan(_chan),
  pad(_pad)
{
  hgmean=0.;
  hgrms=0.;
  lgmean=0.;
  lgrms=0.;
  chancount=0;
  hg.clear();
  lg.clear();
}

correlationAnalyzer::correlationAnalyzer()
{
  m_file=NULL;
  m_hcorr=NULL;
}

correlationAnalyzer::correlationAnalyzer(std::string outputDir, std::string outputname, std::string mapfile)
{
  std::ostringstream os(std::ostringstream::ate);
  os.str("");
  os << outputDir << "/" << outputname << ".root";
  m_file=new TFile(os.str().c_str(),"RECREATE");

  FILE* f = fopen(mapfile.c_str(), "r");
  if (f == 0) {
    fprintf(stderr, "Unable to open '%s'\n", mapfile.c_str() );
    exit(1);
  }
  char buffer[100];
  while (!feof(f)) {
    buffer[0] = 0;
    fgets(buffer, 100, f);
    char* p_comment = index(buffer, '#');
    if (p_comment != 0) *p_comment = 0;
    int ptr;
    int padID, chipID, chanID;
    const char* process = buffer;
    int found = sscanf(buffer, "%d %d %d %n ", &padID, &chipID, &chanID, &ptr);
    if (found == 3) {
      process += ptr;
    } else continue;
    int key=100*chipID+chanID;
    if( m_chanMap.find(key)==m_chanMap.end() ){
      channel_data channel=channel_data(chipID,chanID,padID);
      m_chanMap.insert( std::pair<int,channel_data>(key,channel) );
    }
    else
      std::cout << "WARNING: pad " << padID << "was found twice in " << mapfile << std::endl;
  }
  
}

correlationAnalyzer::~correlationAnalyzer()
{
  if( NULL!=m_file ){
    m_file->Write();
    m_file->Close();
  }
}


void correlationAnalyzer::fillData(std::vector<triggerHit>& hits)
{
  for( auto hit: hits){
    int key=hit.chip()*100+hit.channel();
    if( m_chanMap.find(key)==m_chanMap.end() ) continue;
    m_chanMap[key].hg.push_back(hit.highGainADC(0));
    m_chanMap[key].lg.push_back(hit.lowGainADC(0));
    m_chanMap[key].hgmean+=hit.highGainADC(0);
    m_chanMap[key].lgmean+=hit.lowGainADC(0);
    m_chanMap[key].hgrms+=hit.highGainADC(0)*hit.highGainADC(0);
    m_chanMap[key].lgrms+=hit.lowGainADC(0)*hit.lowGainADC(0);
  }
}

void correlationAnalyzer::analyze()
{
  m_hcorr = new TH2D("hcorr","correlation",m_chanMap.size(),0,m_chanMap.size(),m_chanMap.size(),0,m_chanMap.size());
  m_hcorr->SetMaximum(1);
  m_hcorr->SetMinimum(-1);
  m_hcorr->SetOption("colz");
    
  m_lcorr = new TH2D("lcorr","correlation",m_chanMap.size(),0,m_chanMap.size(),m_chanMap.size(),0,m_chanMap.size());
  m_lcorr->SetMaximum(1);
  m_lcorr->SetMinimum(-1);
  m_lcorr->SetOption("colz");

  int counter(0);
  for( auto &channel:m_chanMap | boost::adaptors::map_values ){ //that's a way of looping and modifying the map content
    unsigned int size = channel.hg.size();
    channel.hgmean = channel.hgmean/size;
    channel.lgmean = channel.lgmean/size;
    channel.hgrms = std::sqrt( channel.hgrms/size - channel.hgmean*channel.hgmean );
    channel.lgrms = std::sqrt( channel.lgrms/size - channel.lgmean*channel.lgmean );
    channel.chancount=counter;
    // std::cout << channel.chip << " " << channel.chan << " "
    // 	      << channel.hgmean << " " << channel.hgrms << " "
    //   	      << channel.lgmean << " " << channel.lgrms << std::endl;
    counter++;
  }

  while( !m_chanMap.empty() ){
    auto channel = m_chanMap.begin()->second;
    unsigned int size = channel.hg.size();
    for( auto it : m_chanMap ){
      auto channel2 = it.second;
      float meanhghg(0),meanlglg(0);
      for( unsigned int ievt=0; ievt<size; ievt++ ){
    	meanhghg+=channel.hg.at(ievt)*channel2.hg.at(ievt);
    	meanlglg+=channel.lg.at(ievt)*channel2.lg.at(ievt);
      }
      meanhghg/=size;
      meanlglg/=size;
      float corrHGHG = (meanhghg-channel.hgmean*channel2.hgmean)/(channel.hgrms*channel2.hgrms);
      float corrLGLG = (meanlglg-channel.lgmean*channel2.lgmean)/(channel.lgrms*channel2.lgrms);
      
      // std::cout << channel.chip << " " << channel.chan << " "
      // 		<< channel2.chip << " " << channel2.chan << " "
      // 		<< corrHGHG << " " << meanhghg << " " << channel.hgmean << " " << channel.hgrms << " " << channel2.hgmean << " " << channel2.hgrms << std::endl;
      
      m_hcorr->Fill( channel.chancount,channel2.chancount,corrHGHG );
      m_lcorr->Fill( channel.chancount,channel2.chancount,corrLGLG );
      if( channel.chancount!=channel2.chancount ){
	m_hcorr->Fill( channel2.chancount,channel.chancount,corrHGHG );
	m_lcorr->Fill( channel2.chancount,channel.chancount,corrLGLG );
      }      
    }
    m_chanMap.erase(m_chanMap.begin());
  }
}


