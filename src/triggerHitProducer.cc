#include "triggerHitProducer.h"

triggerHitProducer::triggerHitProducer( std::string pedestalFileName )
{
  pedestalArchiver par;
  par.loadPedestalData( pedestalFileName, m_pedData );
  // for( auto pcd : m_pedData.get() )
  //   std::cout << pcd << std::endl;

  m_adcHigh.reserve(NUMBER_OF_TIME_SAMPLES);
  m_adcLow.reserve(NUMBER_OF_TIME_SAMPLES);
  for(int it=0; it<NUMBER_OF_TIME_SAMPLES; it++ ){
    m_adcHigh.push_back(0);
    m_adcLow.push_back(0);
  }
}

void triggerHitProducer::produceTriggerHits( std::vector<skiroc2cms>& skirocs, std::vector<triggerHit>& hits )
{
  int chip=0;
  for( auto skiroc : skirocs ){
    std::vector<int> rollpositions=skiroc.rollPositions();
    for( unsigned int ichan=0; ichan<N_CHANNELS_PER_SKIROC; ichan++ ){
      auto pedestals = m_pedData.get().at(chip*N_CHANNELS_PER_SKIROC+ichan);
      for( unsigned int isca=0; isca<NUMBER_OF_SCA; isca++ ){
	if( rollpositions[isca]>=NUMBER_OF_TIME_SAMPLES ) continue;
	m_adcHigh.at( rollpositions[isca] ) = skiroc.ADCHigh(ichan,isca) - pedestals.getPedestal(1,isca);
	m_adcLow.at( rollpositions[isca] ) = skiroc.ADCLow(ichan,isca) - pedestals.getPedestal(0,isca);
      }
      triggerHit hit( chip, ichan, m_adcHigh, m_adcLow, 
		      skiroc.TOARise(ichan), 
		      skiroc.TOAFall(ichan), 
		      skiroc.TOTSlow(ichan), 
		      skiroc.TOTFast(ichan) ) ;
      hits.push_back(hit);
    }
    chip++;
  }
}

