#include <sstream>
#include <cstring>
#include "ntupler.h"

ntupler::ntupler()
{
  m_file=NULL;
  m_outtree=NULL;
}

ntupler::ntupler(std::string outputDir, std::string outputname, std::string treename, std::string description, std::string mapfile)
{
  std::ostringstream os(std::ostringstream::ate);
  os.str("");
  os << outputDir << "/" << outputname << ".root";
  m_file=new TFile(os.str().c_str(),"RECREATE");
  m_outtree=new TTree(treename.c_str(),description.c_str());

  FILE* f = fopen(mapfile.c_str(), "r");
  if (f == 0) {
    fprintf(stderr, "Unable to open '%s'\n", mapfile.c_str() );
    exit(1);
  }
  char buffer[100];
  while (!feof(f)) {
    buffer[0] = 0;
    fgets(buffer, 100, f);
    char* p_comment = index(buffer, '#');
    if (p_comment != 0) *p_comment = 0;
    int ptr;
    int padID, chipID, chanID;
    const char* process = buffer;
    int found = sscanf(buffer, "%d %d %d %n ", &padID, &chipID, &chanID, &ptr);
    if (found == 3) {
      process += ptr;
    } else continue;
    m_padMap.insert( std::pair<int,int>(100*chipID+chanID,padID) );
  }

}

ntupler::~ntupler()
{
  if( NULL!=m_file ){
    m_file->Write();
    m_file->Close();
  }
}

rawntupler::rawntupler() :
  ntupler()
{;}

rawntupler::rawntupler(std::string outputDir, std::string outputname, std::string treename, std::string description, std::string mapfile) : 
  ntupler(outputDir, outputname, treename, description, mapfile) 
{
  m_outtree->Branch("event", &m_event);
  m_outtree->Branch("chip", &m_chip);
  m_outtree->Branch("roll", &m_roll);
  m_outtree->Branch("dacinj", &m_dacinj);
  m_outtree->Branch("timesamp", &m_timesamp,"timesamp[13]/I");
  m_outtree->Branch("hg", &m_hg,"hg[13][64]/I");
  m_outtree->Branch("lg", &m_lg,"lg[13][64]/I");
  m_outtree->Branch("tot_fast", &m_tot_fast,"tot_fast[64]/I");
  m_outtree->Branch("tot_slow", &m_tot_slow,"tot_slow[64]/I");
  m_outtree->Branch("toa_rise", &m_toa_rise,"toa_rise[64]/I");
  m_outtree->Branch("toa_fall", &m_toa_fall,"toa_fall[64]/I");

  m_event=0;
}

void rawntupler::fill(std::vector<skiroc2cms>& skirocs)
{
  int iski=0;
  for( auto skiroc : skirocs ){
    m_chip=iski;
    m_roll=skiroc.rollMask();
    //m_dacinj=skiroc.dacInjection();
    std::vector<int> ts=skiroc.rollPositions();
    int its=0;
    for( std::vector<int>::iterator it=ts.begin(); it!=ts.end(); ++it ){
      m_timesamp[its]=(*it);
      its++;
    }
    for( size_t ichan=0; ichan<N_CHANNELS_PER_SKIROC; ichan++ ){
      for( size_t it=0; it<NUMBER_OF_SCA; it++ ){
	m_hg[it][ichan]=skiroc.ADCHigh(ichan,it);
	m_lg[it][ichan]=skiroc.ADCLow(ichan,it);
      }
      m_tot_fast[ichan]=skiroc.TOTFast(ichan);
      m_tot_slow[ichan]=skiroc.TOTSlow(ichan);
      m_toa_rise[ichan]=skiroc.TOARise(ichan);
      m_toa_fall[ichan]=skiroc.TOAFall(ichan);
    }
    m_outtree->Fill();
    iski++;
  }
  m_event++;
}

triggerhitntupler::triggerhitntupler() :
  ntupler()
{;}

triggerhitntupler::triggerhitntupler(std::string outputDir, std::string outputname, std::string treename, std::string description, std::string mapfile) : 
  ntupler(outputDir, outputname, treename, description, mapfile) 
{
  m_outtree->Branch("event", &m_event);
  m_outtree->Branch("nhit", &m_nhit);
  m_outtree->Branch("chip", &m_chip,"chip[256]/I");
  m_outtree->Branch("channel", &m_channel,"channel[256]/I");
  m_outtree->Branch("pad", &m_pad,"pad[256]/I");
  m_outtree->Branch("hg", &m_hg,"hg[11][256]/I");
  m_outtree->Branch("lg", &m_lg,"lg[11][256]/I");
  m_outtree->Branch("totfast", &m_totfast,"totfast[256]/I");
  m_outtree->Branch("totslow", &m_totslow,"totslow[256]/I");
  m_outtree->Branch("toarise", &m_toarise,"toarise[256]/I");
  m_outtree->Branch("toafall", &m_toafall,"toafall[256]/I");

  m_event=0;
}

void triggerhitntupler::fill(std::vector<triggerHit>& hits)
{
  int hitid=0;
  m_nhit=0;
  for( auto hit : hits ){

    m_chip[hitid]=hit.chip();
    m_channel[hitid]=hit.channel();

    if( m_padMap.find(100*m_chip[hitid]+m_channel[hitid])!=m_padMap.end() )
      m_pad[hitid]=m_padMap[100*m_chip[hitid]+m_channel[hitid]];
    else
      m_pad[hitid]=-1;

    for( unsigned int it=0; it<NUMBER_OF_TIME_SAMPLES; it++ ){
      m_hg[it][hitid] = hit.highGainADC(it);
      m_lg[it][hitid] = hit.lowGainADC(it);
    }

    m_nhit += m_pad[hitid]>-1 && m_hg[3][hitid]+m_hg[4][hitid]-0.6*m_hg[6][hitid]>40 ? 1 : 0 ;

    m_totfast[hitid]=hit.totFast();
    m_totslow[hitid]=hit.totSlow();
    m_toarise[hitid]=hit.toaRise();
    m_toafall[hitid]=hit.toaFall();
    hitid++;
  }
  m_outtree->Fill();
  m_event++;
}
