#include <iostream>
#include <map>

#include "triggerHit.h"

class triggerhit_data{
public:
  triggerhit_data(){;}
 triggerhit_data(int akey) : key(akey),
    medianHG(0.),
    medianLG(0.),
    iqrHG(0.),
    iqrLG(0.){;}

  int key; // chip*100*100 + channel*100 + TS
  std::vector<float> highGain;
  std::vector<float> lowGain;

  float medianHG;
  float medianLG;
  float iqrHG;
  float iqrLG;
};

class noiseAnalyzer
{
 public:
  noiseAnalyzer(){;}
  noiseAnalyzer(std::string outputDir, std::string outputname, std::string mapfile);
  ~noiseAnalyzer(){;}
  void fillData( const std::vector<triggerHit>& hits );
  void analyze();
  //void writeForHexPlot();
  
 private:
  std::map<int,triggerhit_data> triggerhit_map;
  int m_maxTS;
  std::string m_noiseFileForHexPlot;
  struct chanID_to_padID
  {
    int chanID;
    int padID;
    int chipID;
  };
  std::map<int,chanID_to_padID> m_padMap;

};
