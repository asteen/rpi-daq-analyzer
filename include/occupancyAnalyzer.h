#include <iostream>
#include <map>
#include <vector>

#include "skiroc2cms.h"
#include "triggerHit.h"

class hit_channel_data{
public:
  hit_channel_data(){;}
  hit_channel_data(int akey) : key(akey){;}

  int key; // chip*100 + channel
  std::vector<std::vector<int>> highGain;

};

class occupancyAnalyzer
{
 public:
  occupancyAnalyzer(){;}
  occupancyAnalyzer(std::string outputDir, std::string outputname, std::string mapfile, int threshold, int expectedMaxTS, int maxHitsPerEv);//std::string outputname, std::string mapfile, int maxTS);
  ~occupancyAnalyzer(){;}
  void fillData(std::vector<triggerHit> hits);
  void analyze();
//   void writeForHexPlot();
  
 private:
  std::map<int,hit_channel_data> hit_data_map;
  std::map<int,int> m_padMap;
  int m_threshold;
  int m_expectedMaxTS;
  int m_maxHitsPerEv;
  std::string m_occupancyFile,m_occupancyFileForHexPlot;
  std::vector<triggerHit> m_hits;
};
