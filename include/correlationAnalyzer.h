#include <iostream>
#include <map>
#include <TFile.h>
#include <TH2D.h>


#include "triggerHit.h"

class channel_data
{
 public:
  channel_data();
  channel_data(int _chip,int _chan, int _pad);
  
  int chip;
  int chan;
  int pad;
  int chancount;
  std::vector<float> hg;
  std::vector<float> lg;
  
  float hgmean,hgrms,lgmean,lgrms;
};

class correlationAnalyzer
{
 public:
  correlationAnalyzer();
  correlationAnalyzer(std::string outputDir, std::string outputname, std::string mapfile);
  ~correlationAnalyzer();
  void fillData(std::vector<triggerHit>& hits);
  void analyze();
 protected:
  TFile* m_file;
  TH2D* m_hcorr;
  TH2D* m_lcorr;
  std::map<int,channel_data> m_chanMap; //key=100*chip+chan
};
