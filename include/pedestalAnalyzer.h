#ifndef PEDESTAL_ANALYZER
#define PEDESTAL_ANALYZER

#include <iostream>
#include <map>

#include "skiroc2cms.h"

class sca_channel_data{
public:
  sca_channel_data(){;}
 sca_channel_data(int akey) : key(akey),
    medianHG(0.),
    medianLG(0.),
    iqrHG(0.),
    iqrLG(0.){;}

  int key; // chip*100*100 + channel*100 + sca
  std::vector<uint16_t> highGain;
  std::vector<uint16_t> lowGain;

  float medianHG;
  float medianLG;
  float iqrHG;
  float iqrLG;
};

class pedestalAnalyzer
{
 public:
  pedestalAnalyzer(){;}
  pedestalAnalyzer(std::string outputDir, std::string outputname, std::string mapfile, int maxTS);
  ~pedestalAnalyzer(){;}
  void fillData( const std::vector<skiroc2cms>& skirocs );
  void analyze();
  //void writeForHexPlot();
  
 private:
  std::map<int,sca_channel_data> sca_data_map;
  int m_maxTS;
  std::string m_pedestalFile,m_pedestalFileForHexPlot;
  struct chanID_to_padID
  {
    int chanID;
    int padID;
    int chipID;
  };
  std::map<int,chanID_to_padID> m_padMap;

};


#endif
