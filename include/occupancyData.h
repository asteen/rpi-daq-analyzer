#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <utility>
#include <iomanip>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>

#include "skiroc2cms.h"

class occupancyChanData{
 public:
  occupancyChanData(){;}
  occupancyChanData(int chip, int channel);
  occupancyChanData(int chip, int channel, int sensorpad);
  
  inline int chip()      const { return m_chip; }
  inline int channel()   const { return m_channel; }
  inline int sensorpad() const { return m_sensorpad; }
  
  float getNHits()              { return m_nhits; }
  void  setNHits(int value)	{ m_nhits=value; }
  
  bool operator==(const occupancyChanData& occdata)
  {
    return m_chip==occdata.chip() && m_channel==occdata.channel() ;
  }
  
  friend std::ostream& operator<<(std::ostream& out,const occupancyChanData& p)
  {
    out << "Chip,channel,sensorpad " << std::dec << p.m_chip << " " << p.m_channel << " " << p.m_sensorpad << "\t";
    out << std::setprecision(3) ;
//     for(int i=0; i<NUMBER_OF_SCA; i++)
//       out << " " << p.m_occupancyHG[i] ;
//     out << " ";
    return out;
  }

 protected:
  int m_chip;
  int m_channel;
  int m_sensorpad;
  int m_nhits;

 private:
//   friend class boost::serialization::access;
//   template<class Archive>
//     void serialize(Archive & ar, const unsigned int version)
//     {
//       ar & m_chip;
//       ar & m_channel;
//       ar & m_sensorpad;
//       ar & m_occupancyHG;
//       ar & m_noiseHG;
//       ar & m_occupancyLG;
//       ar & m_noiseLG;
//     }
  
};

class occupancyData{
 public:
  occupancyData(){;}
  void fillData(occupancyChanData ocd){ m_datas.push_back(ocd); }
  void fillData(int chip, int channel, int sensorpad);
  void update(occupancyChanData pcd);
  occupancyChanData&              get(int chip,int channel);
  std::vector<occupancyChanData>& get(){return m_datas;}
  bool                            contain(int chip,int channel);
  
 protected:
  std::vector<occupancyChanData> m_datas;

 private:
//   friend class boost::serialization::access;
//   template<class Archive>
//     void serialize(Archive & ar, const unsigned int version)
//     {
//       ar & m_datas;
//     }
  
};

// class occupancyArchiver
// {
//  public:
//   occupancyArchiver(){;}
//   static void savePedestalData( const occupancyData &data, const std::string output)
//   {
//     std::ofstream ofs(output);
//     boost::archive::binary_oarchive oa(ofs);
//     oa << data;
//   }  
//   static void loadPedestalData( const std::string input , occupancyData &data)
//   {
//     std::ifstream ifs( input );
//     boost::archive::binary_iarchive ia(ifs);
//     ia >> data;
//   }  
// };

