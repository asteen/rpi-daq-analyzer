#ifndef TRIGGERHIT
#define TRIGGERHIT 1

#include <iostream>
#include <vector>

const static int NUMBER_OF_TIME_SAMPLES = 11;

class triggerHit
{
 public:
  triggerHit( ){;}
  triggerHit( uint16_t chip, uint16_t channel, std::vector<float> &adcHigh, std::vector<float> &adcLow, float toaRise, float toaFall, float totSlow, float totFast ) ;

  uint16_t chip() const{return m_chip;}
  uint16_t channel() const{return m_channel;}
  
  void setHighGainADCs(std::vector<float> vec){m_adcHigh=vec;}
  void setLowGainADCs(std::vector<float> vec){m_adcLow=vec;}
  void setHighGainADC(int timeSample, float val){m_adcHigh.at(timeSample)=val;}
  void setLowGainADC(int timeSample, float val){m_adcLow.at(timeSample)=val;}
  float highGainADC(int timeSample) const {return m_adcHigh.at(timeSample);}
  float lowGainADC(int timeSample) const {return m_adcLow.at(timeSample);}
  std::vector<float> &highGainADCs(){return m_adcHigh;}
  std::vector<float> &lowGainADCs(){return m_adcLow;}

  void setToaRise(float val){m_toaRise=val;}
  void setToaFall(float val){m_toaFall=val;}
  void setTotFast(float val){m_totFast=val;}
  void setTotSlow(float val){m_totSlow=val;}
  float toaRise(){return m_toaRise;}
  float toaFall(){return m_toaFall;}
  float totFast(){return m_totFast;}
  float totSlow(){return m_totSlow;}

  friend std::ostream& operator<<(std::ostream& s, const triggerHit& hit)
  {
    s << "Chip, channel : " << hit.chip() << ", " << hit.channel() << "\n";
    s << "High gain ADC => " ;
    for (size_t i = 0; i < NUMBER_OF_TIME_SAMPLES; i++)
      s << hit.highGainADC(i) << " ";
    s << "Low gain ADC => " ;
    for (size_t i = 0; i < NUMBER_OF_TIME_SAMPLES; i++)
      s << hit.lowGainADC(i) << " ";
    s << std::endl;
    return s;
  }

 private:
  uint16_t m_chip;
  uint16_t m_channel;
  std::vector<float> m_adcHigh;
  std::vector<float> m_adcLow;
  float m_toaRise;
  float m_toaFall;
  float m_totSlow;
  float m_totFast;
};

#endif
