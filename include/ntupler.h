#include <iostream>
#include <map>
#include <TFile.h>
#include <TTree.h>

#include "skiroc2cms.h"
#include "triggerHit.h"

class ntupler
{
 public:
  ntupler();
  ntupler(std::string outputDir, std::string outputname, std::string treename, std::string description, std::string mapfile);
  ~ntupler();
 protected:
  TFile* m_file;
  TTree* m_outtree;
  std::map<int,int> m_padMap;
};

class rawntupler : public ntupler
{ 
 public:
  rawntupler();
  rawntupler(std::string outputDir, std::string outputname, std::string treename, std::string description, std::string mapfile);

  void fill(std::vector<skiroc2cms> &skirocs);

 private:
  int m_event;
  int m_chip;
  int m_roll;
  int m_dacinj;
  int m_timesamp[NUMBER_OF_SCA];
  int m_hg[NUMBER_OF_SCA][N_CHANNELS_PER_SKIROC];
  int m_lg[NUMBER_OF_SCA][N_CHANNELS_PER_SKIROC];
  int m_tot_fast[N_CHANNELS_PER_SKIROC];
  int m_tot_slow[N_CHANNELS_PER_SKIROC];
  int m_toa_rise[N_CHANNELS_PER_SKIROC];
  int m_toa_fall[N_CHANNELS_PER_SKIROC];

};

class triggerhitntupler : public ntupler
{ 
 public:
  triggerhitntupler();
  triggerhitntupler(std::string outputDir, std::string outputname, std::string treename, std::string description, std::string mapfile);

  void fill(std::vector<triggerHit> &hits);

 private:
  int m_event;
  int m_nhit;
  int m_chip[N_CHANNELS_PER_SKIROC*4];
  int m_channel[N_CHANNELS_PER_SKIROC*4];
  int m_pad[N_CHANNELS_PER_SKIROC*4];
  int m_hg[NUMBER_OF_TIME_SAMPLES][N_CHANNELS_PER_SKIROC*4];
  int m_lg[NUMBER_OF_TIME_SAMPLES][N_CHANNELS_PER_SKIROC*4];
  int m_totfast[N_CHANNELS_PER_SKIROC*4];
  int m_totslow[N_CHANNELS_PER_SKIROC*4];
  int m_toarise[N_CHANNELS_PER_SKIROC*4];
  int m_toafall[N_CHANNELS_PER_SKIROC*4];

};
